#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <winsock2.h>
#include <Windows.h>

#define BUF_SIZE 30

typedef struct Signal {
	int count;
	int max;
	int min;
	int sum;
	int last;
} Signal;

void ErrorHandling(char *message);

int main(int argc, char *argv[])
{
	WSADATA wsaData;
	SOCKET servSock;
	char message[BUF_SIZE];
	int strLen;
	int clntAdrSz;
	int clntCount;
	int i, j;

	SOCKADDR_IN servAdr, clntAdr;
	SOCKADDR_IN *clntAdrs, *clntAdrTmp;
	int *maxValues;
	Signal *signals;

	if (WSAStartup(MAKEWORD(2, 2), &wsaData) != 0)
		ErrorHandling("WSAStartup() error!");

	servSock = socket(PF_INET, SOCK_DGRAM, 0);
	if (servSock == INVALID_SOCKET)
		ErrorHandling("UDP socket creation error");

	memset(&servAdr, 0, sizeof(servAdr));
	servAdr.sin_family = AF_INET;
	servAdr.sin_addr.s_addr = htonl(INADDR_ANY);
	servAdr.sin_port = htons(9000);

	if (bind(servSock, (SOCKADDR*)&servAdr, sizeof(servAdr)) == SOCKET_ERROR)
		ErrorHandling("bind() error");

	printf("input clinet count : ");
	scanf("%d", &clntCount);
	clntAdrs = (SOCKADDR_IN *)malloc(sizeof(SOCKADDR_IN)* clntCount);
	clntAdrTmp = clntAdrs;
	maxValues = (int *)malloc(sizeof(int)* clntCount);
	signals = (Signal *)malloc(sizeof(Signal)* clntCount);
	memset(clntAdrs, 0, sizeof(SOCKADDR_IN)* clntCount);

	for (i = 0; i < clntCount; i++) {
		signals[i].count = 0;
		signals[i].last = -1;
		signals[i].max = -1;
		signals[i].min = -1;
		signals[i].sum = 0;
	}

	i = 0;
	while (clntAdrTmp != clntAdrs + clntCount) {
		memset(message, 0, sizeof(message));
		clntAdrSz = sizeof(*clntAdrTmp);
		recvfrom(servSock, message, sizeof(message), 0, (SOCKADDR*)clntAdrTmp, &clntAdrSz);
		if (*message == 1) {
			printf("connect!\n");
			*message = i + 1;
			sendto(servSock, message, sizeof(message), 0, (SOCKADDR*)clntAdrTmp, sizeof(clntAdr));
			clntAdrTmp++;
			i++;
		}
	}

	system("cls");
	for (i = 0; i < clntCount; i++) {
		printf("[클라이언트%d]\n", i + 1, inet_ntoa(clntAdrs[i].sin_addr), ntohs(clntAdrs[i].sin_port));
		printf("IP:%s\n", inet_ntoa(clntAdrs[i].sin_addr));
		printf("PORT:%d\n\n", ntohs(clntAdrs[i].sin_port));
	}

	printf("모든 클라이언트 접속 완료!\n");
	system("pause");

	system("cls");
	for (i = 0; i < clntCount; i++) {
		printf("\n[클라이언트%d]의 범위: 0 ~ ", i + 1);
		scanf("%d", (int *)(message + 1));
		maxValues[i] = *((int *)(message + 1));
		*message = 2;
		sendto(servSock, message, sizeof(message), 0, (SOCKADDR*)(clntAdrs + i), sizeof(clntAdr));
	}

	printf("모든 클라이언트 범위 설정 완료!\n");
	system("pause");

	for (i = 0; i < clntCount; i++) {
		*message = 3;
		sendto(servSock, message, sizeof(message), 0, (SOCKADDR*)(clntAdrs + i), sizeof(clntAdr));
	}

	while (1) {
		int clientNum;
		int value;

		recvfrom(servSock, message, sizeof(message), 0, (SOCKADDR*)clntAdrTmp, &clntAdrSz);
		clientNum = *(message + 1) - 1;
		value = *((int *)(message + 2));

		signals[clientNum].last = value;
		signals[clientNum].sum += value;
		signals[clientNum].count++;
		signals[clientNum].max = (value > signals[clientNum].max || signals[clientNum].max == -1) ? value : signals[clientNum].max;
		signals[clientNum].min = (value < signals[clientNum].min || signals[clientNum].min == -1) ? value : signals[clientNum].min;

		if (signals[clientNum].last > maxValues[clientNum]) {
			*message = 5;
			sendto(servSock, message, sizeof(message), 0, (SOCKADDR*)(clntAdrs + clientNum), sizeof(clntAdr));
		}

		system("cls");
		for (i = 0; i < clntCount; i++) {
			printf("[클라이언트%d]\n", i + 1, inet_ntoa(clntAdrs[i].sin_addr), ntohs(clntAdrs[i].sin_port));
			printf("IP : %s\n", inet_ntoa(clntAdrs[i].sin_addr));
			printf("PORT : %d\n", ntohs(clntAdrs[i].sin_port));
			printf("범위 : 0 ~ %d\n", maxValues[i]);
			printf("마지막 수신 : %d\n", signals[i].last);
			printf("최대 : %d\n", signals[i].max);
			printf("최소 : %d\n", signals[i].min);
			printf("평균 : %d\n", (signals[i].count != 0) ? signals[i].sum / signals[i].count : 0);

			if (maxValues[i] < signals[i].last) {
				printf("종료(범위 초과)\n");
			}

			printf("\n\n");
		}

		{
			int key;
			if (kbhit()) {
				key = _getch();
				*message = 5;
				sendto(servSock, message, sizeof(message), 0, (SOCKADDR*)(clntAdrs + key - 49), sizeof(clntAdr));
			}

			*message = 6;
			sendto(servSock, message, sizeof(message), 0, (SOCKADDR*)(clntAdrs + clientNum), sizeof(clntAdr));

			fflush(stdin);
		}
	}

	closesocket(servSock);
	WSACleanup();
	return 0;
}

void ErrorHandling(char *message)
{
	fputs(message, stderr);
	fputc('\n', stderr);
	exit(1);
}